package com.example.administrador.proyecto;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class activity_musica_principal extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_musica_principal);
    }
    public void cargarVentana(View view)
    {
        Intent intent = new Intent(this,activity_nueva_cancion.class);
        startActivity(intent);
    }
}
