package com.example.administrador.proyecto;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MenuPrincipa extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_principa);
    }
    public void cargarLibros(View view)
    {
        Intent intent = new Intent(this,activity_libros_principal.class);
        startActivity(intent);
    }
    public void cargarMusica(View view)
    {
        Intent intent = new Intent(this,activity_musica_principal.class);
        startActivity(intent);
    }
}
